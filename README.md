# Streaming Analytics with Apache Druid

In this workshop, we will learn how to ingest real-time metrics into apache druid for real-time analytics and monitoring.
We are going to stream our computer metrics into Kafka, ingest them with Apache Druid in multiple ways and view them using a dedicated UI. Our main focus will be to use Druid to support our needs.

## Who is the workshop intended for
This workshop is intended for people who want to know and learn more about Apache Druid and real-time metrics and analytics.
For those who do not know apache druid yet, there will be an introductory lecture at the beginning

## Requirements
Please see [requirements](REQUIRENMENTS.md)

## About Myself
I am a backend architect, tech lead, and full-cycle developer. I believe programming to be both art & science as it involves human creative skill and imagination while demanding precision and accuracy. Being one of the backend group leaders at Tikal I have the privilege of mentoring other techies in the realms of backend technologies and coding.
I take great interest in new and emerging technologies and strive to keep up to date technologically which may be challenging at times.

https://www.tikalk.com/company/employee/yoavn/