## Requirements for Apache Druid Workshop

### Apache Druid
1. Download Apache Druid ([link](https://druid.apache.org/downloads.html))
2. Perform Quickstart up to section 3 inclusive ([link](https://druid.apache.org/docs/latest/tutorials/index.html)) just to check that everything works on your computer.

### Apache Kafka
1. Download Apache Kafka ([link](https://kafka.apache.org/downloads))
2. Follow quickstart up to section 2 inclusive ([link](https://kafka.apache.org/quickstart)) just to check that everything works on your computer. 
> Important: During this setup apache druid must not be running otherwise it will interfere with the setup.

### Allegro Turnilo
1. Download and install allegro turnilo as described [here](https://github.com/allegro/turnilo#usage)

### InfluxDB Telegraf
1. Download and install Telegraf ([link](https://docs.influxdata.com/telegraf/v1.17/introduction/getting-started/)). 
> Important: Do not run it as a service. We will run it as a simple program from the command line. 
